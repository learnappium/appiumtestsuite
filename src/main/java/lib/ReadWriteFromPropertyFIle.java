package lib;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadWriteFromPropertyFIle {
	
	  public String FileName=null;
      
	  public ReadWriteFromPropertyFIle(String fileName){
	  this.FileName=fileName;
  }
	
	public static void main(String args[]) throws IOException{
		ReadWriteFromPropertyFIle prop = new ReadWriteFromPropertyFIle("android_config_");
		
		System.out.println(prop.readProperty().getProperty("ip"));
		System.out.println(prop.readProperty().getProperty("port"));
		System.out.println(prop.readProperty().getProperty("deviceName"));
		System.out.println(prop.readProperty().getProperty("platformName"));
	//	prop.readProperty().setProperty("ip", "127.0.0.11");
		
	}
	
	
	
	public Properties readProperty() throws IOException{
	Properties prop = new Properties();
	InputStream input = null;

	String filename = "Utils/"+FileName+".properties";
	input = ReadWriteFromPropertyFIle.class.getClassLoader().getResource(filename).openStream();
	if (input == null) {
		System.out.println("Sorry, unable to find " + filename);
	}

	// load a properties file from class path, inside static method
	
	
	prop.load(input);
	return prop; 
}
	
	public void writeProperty() throws IOException  {  //String key, String value
		String filename = "Utils/"+FileName+".properties";
		FileInputStream in = new FileInputStream(filename);
		Properties props = new Properties();
		props.load(in);
		in.close();

		FileOutputStream out = new FileOutputStream(filename);
		props.setProperty("ip", "122.0.0.55");
		props.store(out, null);
		out.close();  out.close();
    } 
	
	
}
