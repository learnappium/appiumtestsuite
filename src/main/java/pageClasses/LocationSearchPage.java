package pageClasses;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.support.PageFactory;

public class LocationSearchPage {
	
public AndroidDriver<MobileElement> driver;
	
	public LocationSearchPage(AndroidDriver<MobileElement> driver){
		
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), LocationSearchPage.this);
	}
	
	
	@AndroidFindBy(xpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]")
	private MobileElement searchCriteriaTextBox;
	
	
	@AndroidFindBy(xpath="//android.widget.ListView[1]/android.widget.LinearLayout/android.widget.TextView[@text='Locality']//..//android.widget.TextView[@text='Airoli, Navi Mumbai']")
	private MobileElement selectFromSuggestion;
	
	
	@AndroidFindBy(id="com.timesgroup.magicbricks:id/done_button")
	private MobileElement doneButton;
	
	
	public LocationSearchPage enterLocation(){
		searchCriteriaTextBox.sendKeys("navi");	
		return this;
	}
	
	
	public LocationSearchPage selectFromSuggestion(){
		selectFromSuggestion.click();	
		return this;
	}
	
	public FrontPage tapDoneButton(){
		doneButton.click();	
		return new FrontPage(driver);
	}
	
}
