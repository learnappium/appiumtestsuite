package pageClasses;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeftHandMenupage {
	
public AndroidDriver<MobileElement> driver;
	
	public LeftHandMenupage(AndroidDriver<MobileElement> driver){
		
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), LeftHandMenupage.this);
	}
	
	
	/*@FindBy(xpath="//android.widget.TextView[@resource-id='com.timesgroup.magicbricks:id/locality_text_view']")
	private MobileElement locationTextBOx;*/
	

	@FindBy(name="REGISTER NOW")
	private MobileElement registeration;
	
	
	public RegristrationPage tapOnRegister(){
		
		registeration.click();
		return new RegristrationPage(driver);
		
		
		
	}

}
