package pageClasses;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegristrationPage {
public AndroidDriver<MobileElement> driver;
	
	public RegristrationPage(AndroidDriver<MobileElement> driver){
		
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), RegristrationPage.this);
	}
	
	
	/*@FindBy(xpath="//android.widget.TextView[@resource-id='com.timesgroup.magicbricks:id/locality_text_view']")
	private MobileElement locationTextBOx;*/
	

	@FindBy(id="com.timesgroup.magicbricks:id/radio_owner")
	private MobileElement owner;
	
	@FindBy(id="com.timesgroup.magicbricks:id/user_name_edit")
	private MobileElement username;
	
	@FindBy(id="com.timesgroup.magicbricks:id/user_email_edit")
	private MobileElement email;
	
	@FindBy(id="com.timesgroup.magicbricks:id/user_password_edit")
	private MobileElement password;
	
	@FindBy(name="Select City")
	private MobileElement selectCityTextArea;
	
	@FindBy(id="com.timesgroup.magicbricks:id/et_city_expanded_search_locality")
	private MobileElement selectCityTextBox;
	
	@FindBy(name="Navi Mumbai")
	private MobileElement selectCity;
	
	@FindBy(id="com.timesgroup.magicbricks:id/user_mobile_edit")
	private MobileElement mobileNumber;
	
	
	@FindBy(id="com.timesgroup.magicbricks:id/submitButton")
	private MobileElement submitButton;
	
	
	public RegristrationPage tapOnUserType(){
		owner.click();
		return this;
	}
	
	public RegristrationPage enterUsername(String user){
		username.sendKeys(user);
		return this;
	}
	
	
	
	public RegristrationPage enterpass(String pass){
		password.sendKeys(pass);
		return this;
	}
	
	public RegristrationPage enteremail(String useremail){
		email.sendKeys(useremail);
		return this;
	}
	
	public RegristrationPage tapToSelectCity(){
		selectCityTextArea.click();
		return this;
	}
	
	/*public RegristrationPage scrollCity(){
		driver.scrollToExact("Anand").click();
		return this;
	}*/
	
	public RegristrationPage searchCity(){
		selectCityTextBox.sendKeys("Mumbai");
		selectCity.tap(1, 1);
		return this;
	}
	
	public RegristrationPage enterMobile(String mobile){
		mobileNumber.sendKeys(mobile);
		return this;
	}
	
	public FrontPage tapOnSubmit(){
        submitButton.click();
		return new FrontPage(driver);
	}
	
	
	
	
	
	
}
