package pageClasses;


import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
//import static org.testng.Assert.*;

public class FrontPage {
	
	public AndroidDriver<MobileElement> driver;
	
	public FrontPage(AndroidDriver<MobileElement> driver){
		
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), FrontPage.this);
	}
	
	
	/*@FindBy(xpath="//android.widget.TextView[@resource-id='com.timesgroup.magicbricks:id/locality_text_view']")
	private MobileElement locationTextBOx;*/
	

	@FindBy(id="com.timesgroup.magicbricks:id/locality_text_view")
	private MobileElement locationTextBOx;

	
	
	
	@FindBy(id="com.timesgroup.magicbricks:id/searchButton")
	private MobileElement searchButton;
	
	@FindBy(id="com.timesgroup.magicbricks:id/bar_icon_home")
	private MobileElement leftHandMenuIcon;
	
	
	public LocationSearchPage tapOnLocationTextBox(){
		locationTextBOx.click();		
		return new LocationSearchPage(driver);
		
	}
	
	public FrontPage verifySearchButton(){
		//assertTrue(searchButton.isDisplayed());
		return this;
	}
	
	public  LeftHandMenupage taponLeftmenu() throws InterruptedException{
		Thread.sleep(2000L);
		leftHandMenuIcon.click();
		return new LeftHandMenupage(driver);
		
	}

}
