package driver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import lib.ReadWriteFromPropertyFIle;

import org.openqa.selenium.remote.DesiredCapabilities;

public class InitDriver {
	
	public AndroidDriver<MobileElement> driver;
	public DesiredCapabilities cap;
	
	public StartServer server;
	
	public AndroidDriver<MobileElement> initdriver() throws IOException{
		
	  
		ReadWriteFromPropertyFIle prop =new ReadWriteFromPropertyFIle("android_config_");
		
		server = new StartServer();
		
		cap = new DesiredCapabilities();
		
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, prop.readProperty().getProperty("platformName"));
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, prop.readProperty().getProperty("deviceName"));
		cap.setCapability(MobileCapabilityType.APP_ACTIVITY, prop.readProperty().getProperty("unlocker_activity"));
		cap.setCapability(MobileCapabilityType.APP_PACKAGE, prop.readProperty().getProperty("unlocker_package"));
		
		
		//driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);
		
		driver = new AndroidDriver<MobileElement>(server.service(), cap);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		
		boolean isappinstalled =driver.isAppInstalled("com.timesgroup.magicbricks");
		
		System.out.println(isappinstalled);
		
       if(isappinstalled){
    	   System.out.println("App is already installed");
    	   driver.resetApp();
       }else{
    	   System.out.println("app is not installed.. istalling app");
    	   driver.installApp(prop.readProperty().getProperty("apppath"));
       }
       
       System.out.println("lanching app");
      driver.startActivity(prop.readProperty().getProperty("app_package"),prop.readProperty().getProperty("app_activity"));
      // driver.startActivity("com.android.settings","Settings");

return driver;
		
		
	}

}
