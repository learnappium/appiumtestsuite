package driver;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.File;

public class StartServer {
	
	public AppiumDriverLocalService service;
	
	public AppiumDriverLocalService service(){
		/*service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
		.usingDriverExecutable(new File("C:\\Program Files\\nodejs\\node.exe"))
		.withAppiumJS(new File("C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\appium.js"))
		.withIPAddress("127.0.0.1")
		.usingPort(4723)
		.withLogFile(new File("D:\\appiumlog.txt")));*/
		
		
		
		service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
		.withIPAddress("127.0.0.1")
		.usingPort(4726)
		.withLogFile(new File("D:\\appiumlog.txt")));
		
		return service;
		
	}

}
