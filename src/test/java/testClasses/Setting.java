package testClasses;

import org.testng.Assert;
import org.testng.annotations.Test;

import driver.TestConfig;

public class Setting extends TestConfig{
	
	
	
	//@Test
	public void settingTest() throws InterruptedException{
		driver.scrollToExact("Location").click();
		Assert.assertFalse(driver.findElementById("android:id/summary").isEnabled());
		driver.findElementById("com.android.settings:id/switch_widget").click();
		Assert.assertTrue(driver.findElementById("android:id/summary").isEnabled());
		
	}

	@Test
	public void Contacts() throws InterruptedException{
		driver.startActivity("com.android.contacts",".activities.PeopleActivity");
		
		
		

		driver.findElementById("com.android.contacts:id/floating_action_button").click();
	
	try{
		driver.findElementById("com.android.contacts:id/frame").click();
		}catch(Exception e){
			driver.findElementByAccessibilityId("contact photo").click();

		}
	
	driver.findElementByName("Choose photo").click();
	
	driver.findElementByName("Oct 24").click();
	
	driver.findElementByName("Save").click();
		Thread.sleep(5000L);
		
		try{
			driver.findElementById("com.android.contacts:id/frame").click();
			}catch(Exception e){
				driver.findElementByAccessibilityId("contact photo").click();

			}
		Assert.assertTrue(driver.findElementByName("Remove photo").isDisplayed());
		
		
	}
}
