package testClasses;

import org.testng.annotations.Test;

import pageClasses.FrontPage;
import driver.TestConfig;

public class SearchTest extends TestConfig{
	
	
	@Test
	public void searchTest() throws InterruptedException{
		
		System.out.println("test cases started");
		
	new FrontPage(driver)
		.tapOnLocationTextBox()
		.enterLocation()
		.selectFromSuggestion()
		.tapDoneButton()
		.verifySearchButton()
		;		
	}

}
