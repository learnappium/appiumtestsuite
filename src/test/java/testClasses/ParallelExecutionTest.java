package testClasses;

import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParallelExecutionTest {
	
	public AndroidDriver<MobileElement> driver;
	public DesiredCapabilities cap;
	public AppiumDriverLocalService service;
	
	@Parameters({"port","UDID","filepath"})
	@BeforeTest
	public void setup(String port,String udid, String path) throws InterruptedException, MalformedURLException{
		 //int port1 = Integer.parseInt(port);
		 //int bp1 = Integer.parseInt(bp);
		 service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
			.withArgument(GeneralServerFlag.CONFIGURATION_FILE,path)
			//.withArgument(GeneralServerFlag.SESSION_OVERRIDE)
			//.withArgument(GeneralServerFlag.CALLBACK_PORT)
			.withLogFile(new File("D:\\appiumlog.txt")));
			//service.start();
		 cap = new DesiredCapabilities();
			
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			cap.setCapability(MobileCapabilityType.DEVICE_NAME,udid);
			cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.til.magicbricks.activities.SplashActivity");
			cap.setCapability(MobileCapabilityType.APP_PACKAGE, "com.timesgroup.magicbricks");
			
			
			//driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:"+port+"/wd/hub"),cap);
			
			driver = new AndroidDriver<MobileElement>(service, cap);
			 Thread.sleep(15000L);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.findElement(By.xpath("(//android.widget.TextView)[2]")).swipe(SwipeElementDirection.UP, 3);
			
			//driver.findElement(By.xpath("(//android.widget.TextView)[2]")).swipe(direction, offsetFromStartBorder, offsetFromEndBorder, duration);
	 }
	 
	 @Test
	 public void test() throws InterruptedException{
		 Thread.sleep(15000L);
	 }
	 
	 
	 @Test
		public void ListonTapHandling() throws InterruptedException{

			Thread.sleep(3000);
			
			// Pre Condition for getting scroll area
			driver.findElement(By.id("chillitalk.mundio.com:id/btn_signup")).click();
			
			Thread.sleep(3000);
			System.out.println("Clicking " + driver.findElement(By.xpath("(//android.widget.TextView)[2]")).getAttribute("name"));
			driver.findElement(By.xpath("(//android.widget.TextView)[2]")).click();
			
			// Get area for scrolling by getting coordinates using elements displayed on screen
			List<MobileElement> elem = driver.findElements(By.xpath("(//android.widget.TextView)"));
			
			System.out.println("elem.size()  " + elem.size());
			System.out.println("elem.get(1).getLocation().getX()  " + elem.get(1).getLocation().getX() + 
							   "  elem.get(1).getLocation().getY()  " + elem.get(1).getLocation().getY());	
			System.out.println("elem.get(elem.size()-1).getLocation().getX()  " + elem.get(elem.size()-2).getLocation().getX() +
							   "  elem.get(elem.size()-1).getLocation().getY()  " + elem.get(elem.size()-2).getLocation().getY());
			
			// Get Scroll coordinates of first element and second last element
			int x1 = elem.get(1).getLocation().getX();
			int y1 = elem.get(1).getLocation().getY();
			int x2 = elem.get(elem.size()-1).getLocation().getX();
			int y2 = elem.get(elem.size()-1).getLocation().getY();
			TouchAction tAction=new TouchAction(driver);
			
			// Perform scroll using a terminating condition
			
			do  {
			
				tAction
				.press(x1,y1)
				.moveTo(x2,y2)
				.release()
				.perform();

				Thread.sleep(1000);
				
			if (driver.findElements(By.name("Malta (+356)")).size()>0)
				driver.findElement(By.name("Malta (+356)")).click(); // Once scroll is performed. Do target action
			} while(driver.findElements(By.name("Malta (+356)")).size()==0);
			
		
		}
	 
	 
	 
	 
	 
	 
	 
	 
	 @AfterTest(alwaysRun=true)
	 public void afterTest() throws InterruptedException{
		driver.quit();
	 }

}
