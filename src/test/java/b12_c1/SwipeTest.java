package b12_c1;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SwipeTest {
	
public static AppiumDriver<MobileElement> driver;
	
	public static DesiredCapabilities cap;
	
	@BeforeTest
	
	public void setupDriver() throws MalformedURLException{		
		cap = new DesiredCapabilities();		
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "192.168.56.101:5555");
		
		cap.setCapability(MobileCapabilityType.APP, "D:\\Appium\\apkfiles\\naukriApp.appModules.login.apk");
		
		//cap.setCapability(MobileCapabilityType.APP_PACKAGE, "naukriApp.appModules.login");

		
		//cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.naukri.fragments.NaukriSplashScreen");
		
		
		//cap.setCapability("setWebContentsDebuggingEnabled",true);
		
		//cap.setCapability("autoWebview", true);


		
		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);	
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		System.out.println("set completed");
		
	}
	
	@Test
	public void LoginTest(){
		System.out.println("test starting");
		
		driver.findElementById("naukriApp.appModules.login:id/loginOrReg").click();
		
		driver.findElementById("naukriApp.appModules.login:id/et_email").sendKeys("itsmailtoyogi@gmail.com");
		
		driver.findElementById("naukriApp.appModules.login:id/et_password").sendKeys("p7108959");
		
		driver.findElementById("naukriApp.appModules.login:id/bt_login").click();
		
		
	}
	
	@Test
	public void homeTest(){
		
		
		
		
		MobileElement el = driver.findElementById("naukriApp.appModules.login:id/jobsForYouRellayout");
		//driver.swipe(el.SwipeElementDirection.LEFT, 2););
		
		
		el.swipe(SwipeElementDirection.DOWN,1);
		
		
		System.out.println("element swipe");
		
		
		MobileElement el1= driver.findElementByName("Resume");
		MobileElement el2=driver.findElementByName("Contact Details");	
		
		///swipe inside element
		//el1.swipe(SwipeElementDirection.LEFT,2000);
		//el2.swipe(SwipeElementDirection.LEFT,2000);
	
		
		///swipe b/w two element
		
		MobileElement destination= driver.findElementByName("Profile Completion");
		MobileElement   source = driver.findElementById("naukriApp.appModules.login:id/contactDetailsFlagged");		
		
		TouchAction action = new TouchAction(driver);
		action.longPress(source).moveTo(destination).release();
		action.perform();
		
		///swipe by cordinate
		
		
		driver.swipe(678, 270, 318, 678, 2000);
		
		
		driver.findElementById("naukriApp.appModules.login:id/empDetailsFlagged").click();
		
		MobileElement elemet1 = driver.findElementById("naukriApp.appModules.login:id/editorScrollView");

		driver.swipe(678, 270, 318, 678, 2000);
		
		//swipeUpElement1(driver, elemet1, 2000);
		driver.scrollToExact("Worked Till");
	}
}


