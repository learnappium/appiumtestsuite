package b12_c1;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.File;
import java.net.MalformedURLException;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class StartStopServer {
	
	public AppiumDriverLocalService service;
	public AndroidDriver<MobileElement> driver;
	public DesiredCapabilities cap;
	
	@BeforeTest
	public void configserver() throws MalformedURLException{
		/*service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
		.usingDriverExecutable(new File("C:\\Program Files\\nodejs\\node.exe"))
		.withAppiumJS(new File("C:\\Program Files (x86)\\Appium\\node_modules\\appium\\bin\\appium.js"))
		.withIPAddress("127.0.0.1")
		.usingPort(4723)
		.withLogFile(new File("D:\\appiumlog.txt")));*/
		
		
		
		service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
		.withIPAddress("127.0.0.1")
		.usingPort(4726)
		.withLogFile(new File("D:\\appiumlog.txt")));
		
		//service.start();
		
	cap = new DesiredCapabilities();
	
	cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
	cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
	cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.til.magicbricks.activities.SplashActivity");
	cap.setCapability(MobileCapabilityType.APP_PACKAGE, "com.timesgroup.magicbricks");
	
	
	//driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);
	
	driver = new AndroidDriver<MobileElement>(service, cap);
	}
	
@Test
public void test1() throws InterruptedException{
	System.out.println("test starting");
	Thread.sleep(5000L);
	
}

@AfterTest
public void stop(){
	
	driver.quit();
	//service.stop();
	
	
	
	
	
	
		
		
	}
}
