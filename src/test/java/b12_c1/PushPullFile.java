package b12_c1;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PushPullFile {
	
	
	
	
public static AppiumDriver<MobileElement> driver;
	
	public static DesiredCapabilities cap;
	
	@BeforeTest
	
	public void setupDriver() throws MalformedURLException{		
		cap = new DesiredCapabilities();		
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "192.168.56.101:5555");
		
		cap.setCapability(MobileCapabilityType.APP, "D:\\Appium\\apkfiles\\MagicBricksPropertySearch_v6.2.5_apkpure.com.apk");
		
		//cap.setCapability(MobileCapabilityType.APP_PACKAGE, "naukriApp.appModules.login");

		
		//cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.naukri.fragments.NaukriSplashScreen");
		
		
		//cap.setCapability("setWebContentsDebuggingEnabled",true);
		
		//cap.setCapability("autoWebview", true);


		
		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);	
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		System.out.println("set completed");
		
	}
	
	@Test
	public void Newtest() throws InterruptedException{
		
		driver.findElementById("com.timesgroup.magicbricks:id/locality_text_view").click();
		Thread.sleep(2000L);
		
		driver.findElementByName("City, Locality, Project").sendKeys("Mum");
		
		driver.findElementByName("Kharghar, Navi Mumbai").click();
		
		driver.findElementById("com.timesgroup.magicbricks:id/done_button").click();
		
		driver.findElementById("com.timesgroup.magicbricks:id/searchButton").click();
		
		if(driver.findElementById("com.timesgroup.magicbricks:id/mProgress").isDisplayed()){
			Thread.sleep(15000L);
		}
		try{
		driver.findElementByXPath("//android.widget.RelativeLayout[0]/ImageView").click();
		}catch(Exception e){
			
			driver.findElementByClassName("android.widget.RelativeLayout").click();
		}
		 
		driver.findElementByClassName("android.widget.RelativeLayout").click();
		
		
	}
	
	
	@AfterTest
	public void quitDriver(){
		
		driver.quit();
	}
	
	
	

}
