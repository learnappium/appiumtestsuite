package b12_c1;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Class3 {

	public static AppiumDriver<MobileElement> driver; // AndroidDriver

	public static DesiredCapabilities cap;

	@BeforeTest
	public void setup() throws MalformedURLException {

		String filepath = "D://Appium//Training//B-12//EriBank.apk";

		cap = new DesiredCapabilities();

		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME,
				"192.168.56.101:5555");
		cap.setCapability(MobileCapabilityType.APP, filepath);

		driver = new AndroidDriver<MobileElement>(new URL(
				"http://127.0.0.1:4723/wd/hub"), cap);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority=1)
	public void firstTest() throws InterruptedException {

		Thread.sleep(5000L);
		System.out.println("first test executing");		
		String expected = "Login";		
		String actual = driver.findElementById("com.experitest.ExperiBank:id/loginButton").getText();		
		Assert.assertEquals(actual, expected);		
		
		driver.findElementById("com.experitest.ExperiBank:id/usernameTextField")
				.sendKeys("company");

		driver.findElementById("com.experitest.ExperiBank:id/passwordTextField")
				.sendKeys("company");

		driver.tap(1, driver
				.findElementById("com.experitest.ExperiBank:id/loginButton"), 1);
		
		String expectedHome = "Logout";		
		String actualHome = driver.findElementById("com.experitest.ExperiBank:id/logoutButton").getText();		
		Assert.assertEquals(actualHome, expectedHome);
		
		System.out.println("first test executed");
		Thread.sleep(5000L);
	}
	
	
	//@Test(priority=2)
	public void MakePaymentTest() throws InterruptedException{
		
		Thread.sleep(2000L);
		driver.tap(1, driver.findElementById("com.experitest.ExperiBank:id/makePaymentButton"), 1);
		
		String expectedMakePayment = "Send Payment";		
		String actualMakePayment = driver.findElementById("com.experitest.ExperiBank:id/sendPaymentButton").getText();		
		Assert.assertEquals(actualMakePayment, expectedMakePayment);
		
		driver.findElementById("com.experitest.ExperiBank:id/phoneTextField").sendKeys("9920536712");
		driver.findElementById("com.experitest.ExperiBank:id/nameTextField").sendKeys("Arvind");
		
		driver.findElementById("com.experitest.ExperiBank:id/amount").click();
		
		String BeforeSelect = driver.findElementById("com.experitest.ExperiBank:id/countryTextField").getText();
		
		driver.findElementById("com.experitest.ExperiBank:id/countryButton").click();
		
		
		String country ="Greece";
		driver.scrollToExact(country).click();
		
		Thread.sleep(2000L);
		
		String AfterSelect = driver.findElementById("com.experitest.ExperiBank:id/countryTextField").getText();
		
		System.out.println("Before selecting country:"+BeforeSelect +"And After select:"+ AfterSelect);
		
		Assert.assertEquals(AfterSelect, country);
		
		driver.tap(1, driver.findElementById("com.experitest.ExperiBank:id/sendPaymentButton"), 1);
		
		driver.tap(1, driver.findElementById("android:id/button1"),1);		
		
		String expectedHome = "Logout";		
		String actualHome = driver.findElementById("com.experitest.ExperiBank:id/logoutButton").getText();		
		Assert.assertEquals(actualHome, expectedHome);		
		
	}
	
	
	@Test(priority=3)
	
	public void WebViewTest() throws InterruptedException{
		Thread.sleep(2000L);
		
        Set<String> contextWindows = driver.getContextHandles();
		
		for (String contexts : contextWindows) {
			
		System.out.println(contexts);
		
		if (contexts.contains("WEBVIEW")) {
			
		driver.context(contexts);
		}
		}
		
		
		driver.findElement(By.id("Your balance is: 100.00$")).getText();
		
		driver.context("NATIVE_APP");
		
	}

	@AfterTest
	public void quitdriver() {
		driver.tap(1, driver
				.findElementById("com.experitest.ExperiBank:id/logoutButton"),
				1);
		
		String expected = "Login";		
		String actual = driver.findElementById("com.experitest.ExperiBank:id/loginButton").getText();		
		Assert.assertEquals(actual, expected);		
		driver.quit();
	}

}
