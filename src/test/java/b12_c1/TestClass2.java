package b12_c1;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

public class TestClass2 {
	public static AppiumDriver<MobileElement> driver;
	public static DesiredCapabilities cap;

	public static void main(String[] args) throws InterruptedException, MalformedURLException {
		// TODO Auto-generated method stub
		
		
		
			cap = new DesiredCapabilities();		
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "192.168.56.101:5555");
			
			cap.setCapability(MobileCapabilityType.APP, "F:\\Appiumtrainiing\\apks\\MagicBricksPropertySearch_v6.2.5_apkpure.com.apk");
			
			//cap.setCapability(MobileCapabilityType.APP_PACKAGE, "naukriApp.appModules.login");

			
			//cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.naukri.fragments.NaukriSplashScreen");
			
			
			//cap.setCapability("setWebContentsDebuggingEnabled",true);
			
			//cap.setCapability("autoWebview", true);


			
			driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);	
			
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			System.out.println("set completed");
			
		
		
		
			driver.findElementById("com.timesgroup.magicbricks:id/locality_text_view").click();
			Thread.sleep(2000L);
			
			driver.findElementByName("City, Locality, Project").sendKeys("Mum");
			
			driver.findElementByName("Kharghar, Navi Mumbai").click();
			
			driver.findElementById("com.timesgroup.magicbricks:id/done_button").click();
			
			driver.findElementById("com.timesgroup.magicbricks:id/searchButton").click();
		
				driver.tap(2, driver.findElementByClassName("android.widget.RelativeLayout"), 1000);
			
			 
			driver.findElementByClassName("android.widget.RelativeLayout").click();
			
			driver.swipe(981, 3555, 157, 3555, 2000);
		
			driver.quit();
		}

}
